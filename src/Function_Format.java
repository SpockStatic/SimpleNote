import java.awt.*;

public class Function_Format {

    Main main;
    Font arial, comicSansMS, timesNewRoman;
    String selectedFont;

    public  Function_Format(Main main){

        this.main = main;

    }

    public void wordWrap(){

        if(main.wordWrapOn==false){
            main.wordWrapOn=true;
            main.textArea.setLineWrap(true);
            main.textArea.setWrapStyleWord(true);
            main.iWrap.setText("Word Wrap: On");
        }else if(main.wordWrapOn==true){
            main.wordWrapOn=false;
            main.textArea.setLineWrap(false);
            main.textArea.setWrapStyleWord(false);
            main.iWrap.setText("Word Wrap: Off");
        }

    }

    public void createFont(int fontSize){
        arial = new Font("Arial",Font.PLAIN, fontSize);
        comicSansMS = new Font("Comic Sans MS",Font.PLAIN,fontSize);
        timesNewRoman = new Font("Times New Roman",Font.PLAIN,fontSize);

        setFont(selectedFont);
    }

    public void setFont(String font){

        selectedFont = font;

        switch (selectedFont){
            case "Arial":
                main.textArea.setFont(arial);
                break;

            case "Comic Sans MS":
                main.textArea.setFont(comicSansMS);
                break;

            case "Times New Roman":
                main.textArea.setFont(timesNewRoman);
                break;
        }
    }

}
