import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

public class Function_File {

    Main main;
    String fileName;
    String fileAddress;

    public Function_File(Main main) {

        this.main = main;

    }

    public void newFile() {
        main.textArea.setText("");
        main.window.setTitle("New");
        fileName=null;
        fileName=null;
    }

    public void openFile() {
        FileDialog fileDialog = new FileDialog(main.window, "Open", FileDialog.LOAD);
        fileDialog.setVisible(true);

        if (fileDialog.getFile() != null) {
            fileName = fileDialog.getFile();
            fileAddress = fileDialog.getDirectory();
            main.window.setTitle(fileName);

        }

        System.out.println(fileAddress + fileName);

        try {

            BufferedReader br = new BufferedReader(new FileReader(fileAddress + fileName));
            main.textArea.setText("");
            String line = null;
            while ((line = br.readLine()) != null) {
                main.textArea.append(line + "\n");
            }
            br.close();


        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (Exception e) {
            System.out.println("Something wrong");
        }

    }


        public void saveFile(){

        if(fileName==null){
            saveAsFile();
        }else{

            try {
                FileWriter fw = new FileWriter(fileAddress+fileName);
                fw.write(main.textArea.getText());
                main.window.setTitle(fileName);
                fw.close();

            }catch (Exception e){
                System.out.println("Something wrong");
            }

        }




        }

        public void saveAsFile(){

        FileDialog fileDialog = new FileDialog(main.window,"SaveAs",FileDialog.SAVE);
        fileDialog.setVisible(true);
            if(fileDialog.getFile() !=null) {
                fileName = fileDialog.getFile();
                fileAddress = fileDialog.getDirectory();
                main.window.setTitle(fileName);

            }

            try {

                FileWriter fw = new FileWriter(fileAddress+fileName);
                fw.write(main.textArea.getText());
                fw.close();

            }catch (Exception e){
                System.out.println("Someting wrong");
            }


        }

        public void exit(){

        System.exit(0);
        }

    }


