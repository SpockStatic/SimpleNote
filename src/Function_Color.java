import java.awt.*;

public class Function_Color {

    Main main;

    public Function_Color(Main main){
        this.main = main;
    }

    public void changeColor(String color){

        switch (color){

            case "White":
                main.window.getContentPane().setBackground(Color.white);
                main.textArea.setBackground(Color.white);
                main.textArea.setForeground(Color.black);
                break;

            case "Black":
                main.window.getContentPane().setBackground(Color.black);
                main.textArea.setBackground(Color.black);
                main.textArea.setForeground(Color.white);
                break;

            case "Blue":
                main.window.getContentPane().setBackground(new Color(102,178,255));
                main.textArea.setBackground(new Color(102,178,255));
                main.textArea.setForeground(Color.white);
                break;

        }

    }

}
