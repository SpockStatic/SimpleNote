public class Function_Edit {

    Main main;

    public Function_Edit(Main main){
        this.main = main;
    }

    public void undo(){
        main.um.undo();
    }

    public void redo(){
        main.um.redo();
    }

}
